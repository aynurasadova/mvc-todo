import React, {useState} from "react";
import KeyboardArrowDownOutlinedIcon from '@material-ui/icons/KeyboardArrowDownOutlined';
import ClearIcon from '@material-ui/icons/Clear';
import {connect} from "react-redux";

import "./index.css";
import { 
    selectAllTodos, 
    addToLocalStorage,
    clearAllTodos,
} from "../../redux/reducer";

const mapStateToProps = (state) => ({
    todos: selectAllTodos(state)
})

export const FormHeader = connect(mapStateToProps, {
    addToLocalStorage,
    clearAllTodos,
})(({
    todos, 
    addToLocalStorage,
    clearAllTodos,
}) => {
    const [text, setInput] = useState("");
    const empty = todos.length === 0;
    const submitForm = (e) => {
        if(e.key === "Enter") {
        addToLocalStorage(text)
        setInput("")
        }
    }

    return (
        <div className = "form__header">
            <ClearIcon 
                onClick = {() => clearAllTodos()}
                style = {{
                    fontSize: 24, 
                    color: "#808080", 
                    visibility: empty && "hidden"
                }} 
                className = "form__headerClearAll" 
            />
            <input 
            value = {text} 
            onChange = {(e) => setInput(e.target.value)} className = "form__headerInputField" placeholder = "What needs to be done?" 
            onKeyDown = {submitForm} 
            />
        </div>
    )
})