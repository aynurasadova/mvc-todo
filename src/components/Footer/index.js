import React, {useState} from "react";
import { connect } from "react-redux";

import "./index.css"
import { 
  selectAllTodos, 
  clearCompleteds, 
  selectFilterName,
  setFilter,
} from "../../redux/reducer";

const mapStateToProps = (state) => ({
  todos: selectAllTodos(state),
  filterName: selectFilterName(state),
})

export const Footer = connect(mapStateToProps, { 
  clearCompleteds,
  setFilter,
})(({ 
  todos, 
  clearCompleteds,
  filterName,
  setFilter,
}) => {

    let itemsLeft = 0;
    let completedsArray = [];

    //Filter the buttons
    const onFilter = (e, filterName) => {
        e.preventDefault()
        setFilter(filterName)
    }

    //Counting items left too be completed
    todos.map(todo => {
      if(!todo.completed) {
        itemsLeft+=1;
      }
    })

    //Filter completeds to Clear
    todos.map(todo => {
      if(todo.completed) {
        completedsArray.push(todo.id)
      }
    })

    const clearCompleted = (e) =>{ 
      e.preventDefault()
      clearCompleteds(completedsArray)
    }

    return (
      <>
      {
       todos.length !== 0 &&
       <div className = "footer">
          <p>{itemsLeft} items left</p>

          <div className = "form__footerFilterBtnsContainer" >
            <button 
              onClick = {(e) => onFilter(e, "all")}
              className = {`footer__FilterBtn ${filterName === "all" ? "footer__FilterBtnActive" : "footer__FilterBtnHover"}`}
            >All</button>
            <button 
              onClick = {(e) => onFilter(e, "active")}
              className = {`footer__FilterBtn ${filterName === "active" ? "footer__FilterBtnActive" : "footer__FilterBtnHover"}`}
            >Active</button>
            <button 
              onClick = {(e) => onFilter(e, "completed")}
              className = {`footer__FilterBtn ${filterName === "completed" ? "footer__FilterBtnActive" : "footer__FilterBtnHover"}`}
            >Completed</button>
          </div>

          <button onClick = {clearCompleted} className = {`footer__ClearBtn ${completedsArray.length ===0 && "hideBtn"}`}>Clear completed</button>
        </div> 
      }
      </>
        
    )
})