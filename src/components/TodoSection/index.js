import React, { useState, useEffect } from "react";
import CheckSharpIcon from '@material-ui/icons/CheckSharp';
import CloseTwoToneIcon from '@material-ui/icons/CloseTwoTone';
import { connect } from "react-redux";

import { 
  selectAllTodos, 
  getTodosFromLocalStorage, 
  deleteTodo, 
  toggleCompleted,
  selectFilterName,
} from "../../redux/reducer";
import "./index.css"

const mapStateToProps = (state) => ({
  todos: selectAllTodos(state),
  filterName: selectFilterName(state)
});

export const TodoSection = connect(mapStateToProps, { 
  getTodosFromLocalStorage, 
  deleteTodo,
  toggleCompleted,
})(({ 
  todos, 
  getTodosFromLocalStorage, 
  deleteTodo,
  toggleCompleted,
  filterName
}) => {

    useEffect(() => {
      window.addEventListener('storage', () => {
      getTodosFromLocalStorage();
      });
      return getTodosFromLocalStorage();
    }, []);

    const filterAll = filterName === "all";
    const filterActive = filterName === "active";
    const filterCompleted = filterName === "completed";

    const filterTodos = (filterType, id, text, completed, status = true) => {
      return (
        filterType && status &&
            <div key = {id} className = "section">
            <CheckSharpIcon 
              onClick = {() => toggleCompleted(id)} 
              style = {{
                color: completed ? "lightgreen" : "white", 
                borderColor: completed ? "lightgreen" : "rgba(222,222,222,1)", 
                transition: "all ease-in-out 1"
              }} 
              className = "section__TodoStatus"
            />
            <p className = "section__TodoText">{text}</p>
            <CloseTwoToneIcon onClick = {() => deleteTodo(id)} className = "section__TodoDelete" />
          </div>
        
        
      )
    }

    return (
      <>
        {
          todos.map(({id, text, completed}) => (
            filterTodos(filterAll, id, text, completed)
          ))            
        }
        {
          todos.map(({id, text, completed}) => (
            filterTodos(filterActive, id, text, completed, !completed)
          ))            
        }
        {
          todos.map(({id, text, completed}) => (
            filterTodos(filterCompleted, id, text, completed, completed)
          ))            
        }
        
      </>
    )
});