import React from 'react';

import { TodoSection } from './components/TodoSection/';
import { Footer } from './components/Footer';
import { FormHeader } from './components/FormHeader';

function App() {
  
  return (
    <>
      <h1 className = "header">todos</h1>
      <form className = "form">

        <FormHeader />

        <TodoSection />

        <Footer />

      </form>
    </>
  );
}

export default App;
