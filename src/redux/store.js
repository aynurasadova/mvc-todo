import {combineReducers, createStore, applyMiddleware} from "redux";
import { composeWithDevTools } from "redux-devtools-extension";
import thunk from "redux-thunk";

import { MODULE_NAME as todosModuleName, todosReducer} from "./reducer";

const rootReducer = combineReducers({
    [todosModuleName]: todosReducer,
});


const store = createStore(rootReducer, composeWithDevTools(applyMiddleware(thunk)));

export default store;
