const SET_TODO = "SET_TODO";
const SET_FILTER = "SET_FILTER";

export const MODULE_NAME = "todos";
export const selectAllTodos = (state) => state[MODULE_NAME]?.todos;
export const selectFilterName = (state) => state[MODULE_NAME]?.filterName;

const initialState = {
    todos: [],
    filterName: "all",
}

export const todosReducer = (state = initialState, {type, payload}) => {
    switch (type) {
        case SET_TODO:
            return {
                ...state,
                todos: payload
            }
        case SET_FILTER:
            return {
                ...state,
                filterName: payload
            }
        default:
            return state;
    }
}

export const setTodo = (payload) => ({
    type: SET_TODO,
    payload,
});

export const setFilter = (payload) => ({
    type: SET_FILTER,
    payload,
});

//Random id generator
const IDGenerator = () => {
    return `${Math.random()}_${Date.now()}`
}

//Function to get all key and value pairs from localStorage
function allStorage() {

    var values = [],
        keys = Object.keys(localStorage),
        i = keys.length;

    while ( i-- ) {
        values.push( JSON.parse(localStorage.getItem(keys[i])) );
    }

    return values;
}

//After getting storage key and value pairs, assigning them to redux storage
export const getTodosFromLocalStorage = () => (dispatch) => {
    try {
        if(localStorage.length !== 0) {
            dispatch(setTodo(allStorage()))
        } else {
            dispatch(setTodo([]))
        }
    } catch (error) {
        alert("Something went wrong")
    }
}

//Adding new todo to localStorage
export const addToLocalStorage = (text) => () => {
    const id = IDGenerator();
    const todoDetail = {
        id,
        text,
        completed: false,
    }
    localStorage.setItem(`${id}`, JSON.stringify(todoDetail))
}

//Delete single todo by ID
export const deleteTodo = (id) => () => {
    try {
        localStorage.removeItem(`${id}`)
    } catch (error) {
        alert("Something went wrong")
    }
}

//Toggling completed status of todo
export const toggleCompleted = (id) => () => {
    const {text, completed} = JSON.parse(localStorage.getItem(`${id}`));
    const toogledTodo = {
        id,
        text,
        completed: !completed
    }
    // toggledTodo.completed(v => !v)
    localStorage.setItem(`${id}`, JSON.stringify(toogledTodo));

}

//Clearing completed todos from LocalStorage
export const clearCompleteds = (completedsArray) => () => {
    try {
        const arrayLength = completedsArray.length;
        if(arrayLength) {
            for(let i = 0; i < arrayLength; i++) {
                localStorage.removeItem(completedsArray[i])
            }
        }
    } catch (error) {
        alert("Something went wrong")
    }
}

export const clearAllTodos = () => () => {
    try {
        localStorage.clear()
    } catch (error) {
        alert("Something went wronn")
    }
}